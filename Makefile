.PHONY: clean-pyc clean-build docs

help:
	@echo "clean-build - remove build artifacts"
	@echo "clean-pyc - remove Python file artifacts"
	@echo "lint - check style with flake8"
	@echo "test - run tests quickly with the default Python"
	@echo "testall - run tests on every Python version with tox"
	@echo "coverage - check code coverage quickly with the default Python"
	@echo "docs - generate Sphinx HTML documentation, including API docs"
	@echo "release - package and upload a release"
	@echo "sdist - package"

env:
	pip install pip --upgrade
	pip install -r './requirements.txt'

static:
	python ./manage.py collectstatic --no-input

init:
	python ./manage.py migrate --no-input
	python ./manage.py collectstatic --no-input
	python ./manage.py createsuperuser

migrations:
	python ./manage.py makemigrations --no-input

migrate:
	python ./manage.py migrate --no-input

server:
	python ./manage.py runserver

messages:
	python ./manage.py makemessages -l fr --ignore=static/* --ignore=env/*

translate:
	python ./manage.py compilemessages -l fr

clean: clean-build clean-pyc

clean-build:
	rm -fr build/
	rm -fr dist/
	rm -fr *.egg-info

clean-pyc:
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +

lint:
	flake8 djangocms-forms tests

test:
	python runtests.py test

test-all:
	tox

coverage:
	coverage run --source djangocms-forms setup.py test
	coverage report -m
	coverage html
	open htmlcov/index.html

docs:
	rm -f docs/djangocms-forms.rst
	rm -f docs/modules.rst
	sphinx-apidoc -o docs/ djangocms-forms
	$(MAKE) -C docs clean
	$(MAKE) -C docs html
	open docs/_build/html/index.html

release: clean
	python setup.py sdist upload
	python setup.py bdist_wheel upload

sdist: clean
	python setup.py sdist
	ls -l dist