import os  # isort:skip
from django.utils.translation import ugettext_lazy as _
from .utils import load_dss

gettext = lambda s: s
DATA_DIR = os.path.dirname(os.path.dirname(__file__))

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

DS_SETTINGS_FILENAME = "settings.dss"

THUMBNAIL_HIGH_RESOLUTION = True
FILE_UPLOAD_PERMISSIONS = 0o644
FILE_UPLOAD_MAX_MEMORY_SIZE = 10000000 #10Mo

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.9/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ.get('SECRET_KEY')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False if str(os.environ.get('DEBUG', "false")) == "false" else True

ALLOWED_HOSTS = [host for host in os.environ.get('ALLOWED_HOSTS', "" if DEBUG == False else "*").replace(", ", ",").split(",")] #[]

# Application definition
ROOT_URLCONF = 'Web_mvc.urls'

# Internationalization
# https://docs.djangoproject.com/en/1.9/topics/i18n/

TIME_ZONE = os.environ.get('TIME_ZONE', "Europe/Zurich")
USE_TZ = True


ADMINS = (
    # ('admin', 'admin@waser.tech'),
)

MANAGERS = ADMINS

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.9/howto/static-files/

STATIC_URL = '/static/'
MEDIA_URL = '/media/'
STATIC_ROOT = os.path.join(DATA_DIR, 'static')
MEDIA_ROOT = os.path.join(DATA_DIR, 'media')

SITE_ID = 1


TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'templates'),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.i18n',
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.template.context_processors.media',
                'django.template.context_processors.csrf',
                'django.template.context_processors.tz',
                'sekizai.context_processors.sekizai',
                'django.template.context_processors.static',
                'cms.context_processors.cms_settings',
            ],
            'libraries': {
#                'sorl_thumbnail':'sorl.thumbnail.templatetags.thumbnail',
                'easy_thumbnails': 'easy_thumbnails.templatetags.thumbnail',
            },
#            'loaders': [
#                'django.template.loaders.filesystem.Loader',
#                'django.template.loaders.app_directories.Loader',
#                'django.template.loaders.eggs.Loader'
#            ],
        },
    },
]


MIDDLEWARE = [
    'cms.middleware.utils.ApphookReloadMiddleware',
    'django.contrib.sites.middleware.CurrentSiteMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'cms.middleware.user.CurrentUserMiddleware',
    'cms.middleware.page.CurrentPageMiddleware',
    'cms.middleware.toolbar.ToolbarMiddleware',
    'cms.middleware.language.LanguageCookieMiddleware'
]

INSTALLED_APPS = [
    'djangocms_admin_style',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.admin',
    'django.contrib.sites',
    'django.contrib.sitemaps',
    'django.contrib.staticfiles',
    'django.contrib.messages',
    'cms',
    'menus',
    'sekizai',
    'treebeard',
    'djangocms_text_ckeditor',
    'filer',
    'easy_thumbnails',
    'djangocms_forms',
    'Web_mvc',
]

LANGS = os.environ.get("LANGUAGES", "fr-fr").replace(", ", ",").split(",")

USE_I18N = True if len(LANGS) > 1 else False

USE_L10N = USE_I18N

LANGUAGES = []
for lang in LANGS:
    LANGUAGES.append((lang, gettext(lang)))

LANGUAGE_CODE = LANGS[0]

THUMBNAIL_PROCESSORS = [
    'easy_thumbnails.processors.colorspace',
    'easy_thumbnails.processors.autocrop',
    'filer.thumbnail_processors.scale_and_crop_with_subject_location',
    'easy_thumbnails.processors.filters',
]

META_SITE_PROTOCOL = 'https'
META_USE_SITES = True

DATABASES =  {
    'default': {
        'ENGINE': "django.db.backends.sqlite3",
        'NAME': os.path.join(DATA_DIR, 'db.sqlite3'),
    }
}

LOCALE_PATHS = [
    os.path.join(BASE_DIR, 'theme/locale'),
]

CMS_LANGUAGES = {
    'default': {
        'redirect_on_fallback': True,
        'public': True,
        'hide_untranslated': False,
    },
}

CMS_LANGUAGES[1] = []

for lg in LANGS:
    CMS_LANGUAGES[1].append({
            'code': lg,
            'hide_untranslated': False,
            'name': lg,
            'public': True,
        })

if len(LANGS) > 1:
    CMS_LANGUAGES[1][1]['redirect_on_fallback'] = True

#DJANGOCMS_FORMS_PLUGIN_NAME = _('Form')
FOBI_DEFAULT_THEME = 'bootstrap3'

STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
]

CRISPY_TEMPLATE_PACK = 'bootstrap4'

DJANGOCMS_FORMS_WIDGET_CSS_CLASSES = {'__all__': ('form-control', ) }

CMS_PERMISSION = bool(os.environ.get('CMS_PERMISSION', False))

CMS_PLACEHOLDER_CONF = {}

FILER_FILE_MODELS = (
      'filer.Image',
      'filer.File',
)



# Dynamicly stored settings
DS_SETTINGS = load_dss(BASE_DIR, DS_SETTINGS_FILENAME)

if DS_SETTINGS != None:
    INSTALLED_APPS += DS_SETTINGS['settings'].get('INSTALLED_APPS')
    LOCALE_PATHS += [os.path.join(BASE_DIR, local_path) for local_path in DS_SETTINGS['settings'].get('LOCALE_PATHS')]
    DJANGOCMS_FORMS_TEMPLATES = DS_SETTINGS['settings'].get('DJANGOCMS_FORMS_TEMPLATES')
    NEWSLETTER_PLUGIN_TEMPLATE_FOLDERS = DS_SETTINGS['settings'].get('NEWSLETTER_PLUGIN_TEMPLATE_FOLDERS')
    TEMPLATES[0]['DIRS'] += [os.path.join(BASE_DIR, local_path) for local_path in DS_SETTINGS['settings']['TEMPLATES'].get('DIRS')]
    TEMPLATES[0]['OPTIONS']['context_processors'] += DS_SETTINGS['settings']['TEMPLATES']['OPTIONS'].get('context_processors', [])
    BLOG_IMAGE_FULL_SIZE = DS_SETTINGS['settings'].get('BLOG_IMAGE_FULL_SIZE')
    BLOG_IMAGE_THUMBNAIL_SIZE = DS_SETTINGS['settings'].get('BLOG_IMAGE_THUMBNAIL_SIZE')
    PORTFOLIO_IMAGE_THUMBNAIL_SIZE = DS_SETTINGS['settings'].get('PORTFOLIO_IMAGE_THUMBNAIL_SIZE')
    BLOG_LATEST_POSTS = DS_SETTINGS['settings'].get('BLOG_LATEST_POSTS')
    BLOG_PLUGIN_TEMPLATE_FOLDERS = DS_SETTINGS['settings'].get('BLOG_PLUGIN_TEMPLATE_FOLDERS')
    CMS_TEMPLATES = DS_SETTINGS['settings'].get('CMS_TEMPLATES')
    CKEDITOR_SETTINGS = DS_SETTINGS['settings'].get('CKEDITOR_SETTINGS')
    DJANGOCMS_ICON_SETS = DS_SETTINGS['settings'].get('DJANGOCMS_ICON_SETS')
    HAS_PURCHASED_THEME_LICENCE = DS_SETTINGS['settings'].get('HAS_PURCHASED_THEME_LICENCE')
